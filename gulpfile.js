// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var fs = require('fs');
var os = require('os');
var autoprefixer = require('gulp-autoprefixer');
var compass = require('gulp-compass');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
// var GulpSSH = require('gulp-ssh');
var rsync = require('gulp-rsync');
var connect = require('gulp-connect-php');

var requireDir = require('require-dir');
const browserSync = require( 'browser-sync' ).create();
requireDir('./gulp-tasks');

var envTest = {
    host: "makeitabetterplace.eu",
    username: "",
    domain: 'makeitabetterplace.eu',
    theme_name: 'makeitabetterplace-theme',
    remote_path: function() {return 'domains/' + this.domain + '/public_html/wp-content/themes/' + this.theme_name},
};

var sshConfig = {
    host: envTest.host,
    port: 22,
    readyTimeout: 99999,
    username: envTest.username,
    privateKey: fs.readFileSync(os.homedir() + '/.ssh/id_rsa')
};

gulp.task('serve', function() {

    browserSync.init({
        server: "./"
    });
    gulp.watch("src/*.php").on('change', browserSync.reload);
}); 

taskSass = gulp.task('sass', function(){
    return gulp.src('sass/**/*.scss')
        .pipe(compass({config_file: 'config.rb'}))
        .on('error', function() {
            this.emit('end');
        })
        .pipe(autoprefixer({
            browsers: ['last 2 versions','ie >= 9'],
            cascade: false
        }))
        .pipe(gulp.dest('css/'))
        .on('end', function() {
            console.log('the end');
        })
});

taskWatch = gulp.task('watch', ['sass'], function() {
    gulp.watch('sass/**/*.scss', ['sass']);
    console.log('watching.. press ctrl + c to leave.');
});

gulp.task('webserver', function() {
    connect.server();
});

taskDeployToTST = gulp.task('deployTST', function() {
    gulp.src('.')
        .pipe(rsync({
            hostname: envTest.host,
            username: envTest.username,
            destination: envTest.remote_path(),
            archive: true,
            compress: true,
            update: true,
            emptyDirectories: true,
            exclude: [
                'node_modules',
                'sass',
                '.git*',
                '.sass-cache',
                '.DS_Store',
                'config.rb',
                'gulpfile.js',
                'package.json'
            ],
            clean: true,
        }));
});

taskDefault = gulp.task('default', ['sass', 'watch']);