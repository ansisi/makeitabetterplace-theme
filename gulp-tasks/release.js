// /**
//  * Created by Superkrypt.
//  * (2016)
//  */

// var gulp = require('gulp');
// var fs = require('fs');
// var runSequence = require('run-sequence');

// var git = require('gulp-git');
// var bump = require('gulp-bump');
// var prompt = require('gulp-prompt');
// var replace = require('gulp-replace');
// var del = require('del');

// var packagejson = JSON.parse(fs.readFileSync('./package.json'));

// gulp.task('release-create', function() {
//     var path = '.';
//     packagejson = JSON.parse(fs.readFileSync('./package.json'));
//     var destination = gulp.dest('release/' + packagejson.name + '-' + packagejson.version);

//     return gulp.src([
//         path+'/**/*',
//         '!'+path+'/{node_modules,node_modules/**}',
//         '!'+path+'/{release,release/**}',
//         '!'+path+'/{sass,sass/**}',
//         '!'+path+'/{test,test/**}',
//         '!'+path+'/{gulp-tasks,gulp-tasks/**}',
//         '!'+path+'/gulpfile.js',
//         '!'+path+'/config.rb',
//         '!'+path+'/*.json',
//         '!'+path+'/composer.json',
//         '!'+path+'/composer.lock',
//         '!'+path+'/release-notes.txt',
//     ])
//         .pipe(replace('$VERSION$', packagejson.version, {
//             skipBinary: true,
//         }))
//         .pipe((destination));
// });

// gulp.task('release-clean', function(callback) {
//     return del(['release/' + packagejson.name + '-' + packagejson.version], callback);
// });

// var bumpType = 'to be changed';
// gulp.task('release-prompt-version', function() {
//     return gulp.src('./package.json', {read: false})
//         .pipe(prompt.prompt({
//             type: 'list',
//             name: 'bump',
//             message: 'What type of version bump would you like to do (last version: ' + packagejson.version + '?',
//             choices: ['patch', 'minor', 'major'],
//         }, function (res) {
//             bumpType = res.bump;
//         }))
// });
// gulp.task('release-bump-version', ['release-prompt-version'], function() {
//     res = gulp.src('./package.json')
//         .pipe(bump({type: bumpType}))
//         .pipe(gulp.dest('./'));
//     return res;
// });
// gulp.task('release-to-git', function() {
//     gulp.src('./package.json')
//         .pipe(git.commit('release version: ' + packagejson.version));

//     git.tag('v' + packagejson.version, 'Release created', function (err) {
//         if (err) throw err;
//         else git.push('origin', 'master', {args: " --tags"}, function (err) {
//             if (err) throw err;
//         });

//     })
// });

// gulp.task('release', function (callback) {
//     runSequence('release-bump-version', 'release-clean', 'release-create', 'release-to-git',  callback);
// });
