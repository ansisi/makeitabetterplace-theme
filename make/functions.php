<?php
define('THEME_DIR', get_template_directory_uri());
define('IMG_DIR', THEME_DIR.'/img');

include "includes/remove-emoji.php";
include "includes/svg-support.php";
include "includes/enqueue_styles_scripts.php";
include "includes/theme-setup.php";

include "includes/choice_of_background.php";
// include "includes/remove_orphans.php";

include "admin/not_limited_posts_on_category.php";
include "admin/polylang_register_strings.php";
