<?php

function makeit_choice_of_background($post_id) {
    switch ($post_id % 4) {
        case 0:
            echo 'red-box';
            break;
        case 1:
            echo 'green-box';
            break;
        case 2:
            echo 'blue-box';
            break;
        case 3:
            echo 'yellow-box';
            break;
    }
}

function makeit_post_thumbnail_background() {
    if (has_post_thumbnail()) {
        $image_id = get_post_thumbnail_id(get_the_ID());
        $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
        echo ' style="background-image: url('; the_post_thumbnail_url();echo ')" role="img" tabindex="-1" aria-label="'; echo $image_alt; echo '"';
    }
    else {
        echo ' data-bg="no-photo"';
    }
}
