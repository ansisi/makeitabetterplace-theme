<?php

function makeit_add_svg_support ( $existing_mimes=array() ) {
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}

add_filter('upload_mimes', 'makeit_add_svg_support');
