<?php

function ipeco_body_class( $classes ) {
    $page_name = get_query_var('pagename');
    $classes[] = $page_name;
    return $classes;
}
