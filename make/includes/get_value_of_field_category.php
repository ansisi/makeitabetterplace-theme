<?php
/**
 * Created by Superskrypt.
 * (2016)
 */

function get_value_of_field($field_name) {
    if (is_category()) :
        $category_id = 'category_'.get_query_var('cat');
        $$field_name = get_field($field_name, $category_id);
    else : 
        $$field_name = get_field($field_name);
    endif;
    return $$field_name;
}