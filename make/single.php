<?php

get_header();

get_template_part("partials/logo");

get_template_part("partials/post-content");

get_template_part("comments");

get_footer();
