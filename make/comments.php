<?php
if ( post_password_required() ) {
	return;
}

$post_id = pll_get_post($post->ID, pll_default_language()); 

$args = array(
    'status' => 'approve',
    'post_id'=> $post_id
);
$comments_query = new WP_Comment_Query;
$comments = $comments_query->query( $args );
?>
<section class="article page-content <?php makeit_choice_of_background($post_id); ?>">
	<div id="comments" class="comments-area">
	<?php if ( $comments ) : ?>
		<h2 class="comments-title">
			<?php pll_e('comments'); ?>
		</h2>

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php foreach( $comments as $comment ):
				if ( $post_id == $comment->comment_post_ID ): ?>

			<?php endif;
				endforeach; ?>
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 42,
				), $comments );
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentysixteen' ); ?></p>
	<?php endif; ?>

	<?php
		comment_form( array(
			'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
			'title_reply_after'  => '</h2>',
		) );
	?>

	</div><!-- .comments-area -->
</section>

