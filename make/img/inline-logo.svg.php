<?php ?>
<svg version="1.1" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
	 y="0px" width="311.833px" height="274px" viewBox="0 0 311.833 274" overflow="scroll" xml:space="preserve">
<g id="Layer_1">
	<path fill="none" d="M232.397,213.384c-16.688,18.819-39.733,29.993-64.89,31.459c-25.157,1.466-49.416-6.951-68.304-23.699
		c-18.888-16.749-30.149-39.825-31.705-64.978c-1.557-25.149,6.777-49.368,23.466-68.188c16.688-18.821,39.734-29.993,64.893-31.461
		c25.156-1.465,49.412,6.952,68.302,23.701c18.889,16.748,30.148,39.824,31.705,64.976
		C257.419,170.347,249.085,194.563,232.397,213.384z"/>
	<text transform="matrix(0.5362 -0.8398 0.8429 0.5381 75.4165 110.0586)" font-family="'Rewir-Light'" font-size="30">m</text>
	<text transform="matrix(0.7062 -0.7029 0.7054 0.7088 89.6519 88.9551)" font-family="'Rewir-Light'" font-size="30">a</text>
	<text transform="matrix(0.8138 -0.5749 0.577 0.8168 101.5913 77.3311)" font-family="'Rewir-Light'" font-size="30">k</text>
	<text transform="matrix(0.8994 -0.4288 0.4303 0.9027 113.9292 68.6875)" font-family="'Rewir-Light'" font-size="30">e</text>
	<text transform="matrix(0.9499 -0.301 0.302 0.9533 129.1685 61.8398)" font-family="'Rewir-Light'" font-size="30"> </text>
	<text transform="matrix(0.9749 -0.206 0.2067 0.9784 137.9194 59.1553)" font-family="'Rewir-Light'" font-size="30">i</text>
	<text transform="matrix(0.9919 -0.0945 0.0948 0.9955 147.019 57.2461)" font-family="'Rewir-Light'" font-size="30">t</text>
	<text transform="matrix(0.9963 0.0161 -0.0161 0.9999 159.2998 56.2617)" font-family="'Rewir-Light'" font-size="30"> </text>
	<text transform="matrix(0.9849 0.1509 -0.1515 0.9885 168.2964 56.2471)" font-family="'Rewir-Light'" font-size="30">a</text>
	<text transform="matrix(0.955 0.2844 -0.2854 0.9584 184.8345 59.1621)" font-family="'Rewir-Light'" font-size="30"> </text>
	<text transform="matrix(0.9045 0.4179 -0.4194 0.9078 193.5063 61.5313)" font-family="'Rewir-Light'" font-size="30">b</text>
	<text transform="matrix(0.8146 0.5738 -0.5758 0.8176 209.4966 69.1221)" font-family="'Rewir-Light'" font-size="30">e</text>
	<text transform="matrix(0.7205 0.6883 -0.6908 0.7231 222.9644 78.9609)" font-family="'Rewir-Light'" font-size="30">t</text>
	<text transform="matrix(0.6269 0.7744 -0.7772 0.6292 231.7417 87.4873)" font-family="'Rewir-Light'" font-size="30">t</text>
	<text transform="matrix(0.5017 0.8609 -0.864 0.5035 239.4312 96.8936)" font-family="'Rewir-Light'" font-size="30">e</text>
	<text transform="matrix(0.3645 0.9274 -0.9307 0.3658 247.5562 111.418)" font-family="'Rewir-Light'" font-size="30">r</text>
	<text transform="matrix(0.2572 0.9626 -0.9661 0.2581 251.8462 122.8408)" font-family="'Rewir-Light'" font-size="30"> </text>
	<text transform="matrix(0.1192 0.9893 -0.9928 0.1196 254.3589 131.4727)" font-family="'Rewir-Light'" font-size="30">p</text>
	<text transform="matrix(-0.0253 0.9961 -0.9997 -0.0254 256.104 149.2354)" font-family="'Rewir-Light'" font-size="30">l</text>
	<text transform="matrix(-0.1666 0.9824 -0.9859 -0.1672 255.9731 159.3965)" font-family="'Rewir-Light'" font-size="30">a</text>
	<text transform="matrix(-0.3293 0.9404 -0.9438 -0.3305 252.9868 175.8027)" font-family="'Rewir-Light'" font-size="30">c</text>
	<text transform="matrix(-0.48 0.8732 -0.8763 -0.4817 248.1606 189.5029)" font-family="'Rewir-Light'" font-size="30">e</text>
</g>
<g id="Layer_3">
	<line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="56.333" y1="186.666" x2="181.333" y2="186.666"/>
	<line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="64.333" y1="195.666" x2="150.333" y2="195.666"/>
	<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M132.333,204.666"/>
	<g display="none">
		<path display="inline" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M226.928,195.666
			c8.464-12.552,13.405-27.674,13.405-43.951c0-43.474-35.241-78.715-78.715-78.715c-43.473,0-78.715,35.241-78.715,78.715
			c0,12.557,2.955,24.419,8.185,34.951"/>
		<path display="inline" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M133.333,195.666H96.309
			c14.141,20.971,38.114,34.764,65.31,34.764c23.083,0,43.845-9.936,58.242-25.764h-86.527"/>
		<polyline display="inline" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" points="227.333,195.666 
			193.333,195.666 193.333,178.666 221.958,178.666 192.271,136.604 179.083,157.666 192.333,157.666 		"/>
		
			<line display="inline" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="132.333" y1="204.666" x2="220.333" y2="204.666"/>
	</g>
	<polyline fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" points="172.903,138.708 150.841,119.771 
		141.833,112.041 86.55,175.421 	"/>
	
		<line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="134.779" y1="138.041" x2="150.841" y2="119.771"/>
	
		<line fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" x1="134.779" y1="156.511" x2="159.933" y2="127.575"/>
	<g>
		<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M90.971,187.025
			c-5.297-10.592-8.214-22.714-8.214-35.362c0-43.515,35.315-78.88,78.831-78.88c5.65,0,11.176,0.562,16.49,1.692
			c-0.654,2.852-1.002,5.793-1.002,8.84c0,21.849,17.779,39.611,39.628,39.611c5.995,0,11.681-1.349,16.779-3.743
			c4.44,9.861,6.934,21.285,6.934,32.802c0,16.375-4.997,31.681-13.548,44.681h-34.535v-17h29.625l-29.625-41.723l-12.873,20.723
			h12.873"/>
		<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M132.333,205.666h87.42
			c-14.405,15-35.111,25.358-58.126,25.358c-27.14,0-51.071-13.974-65.242-34.858l-0.302-0.5"/>
		<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M186.778,76.817
			c-0.45,2.088-0.692,4.253-0.692,6.476c0,16.912,13.71,30.623,30.623,30.623c4.503,0,8.775-0.98,12.626-2.726
			C219.789,95.185,204.73,82.863,186.778,76.817z"/>
		<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M219.333,204.666"/>
		<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M132.333,204.666"/>
	</g>
</g>
</svg>
