<?php
/*
Template Name: Archives
*/

get_header();

get_template_part("partials/logo");

get_template_part("partials/posts-section");

get_footer();