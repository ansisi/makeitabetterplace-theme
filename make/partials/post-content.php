<?php
$post_id = pll_get_post($post->ID, pll_default_language()); 
?>

<section class="article page-content <?php makeit_choice_of_background($post_id); ?>">
 
    <h1 class="post-title">
        <?= get_the_title(); ?>
    </h1>

    <?php get_template_part("partials/post-tags"); ?>

    <div class="article-content">

	    <?php if ( have_posts() ) : 
	        while ( have_posts() ) : the_post();
	            the_content();
	        endwhile;
	    endif; ?>

	  </div>

</section>
