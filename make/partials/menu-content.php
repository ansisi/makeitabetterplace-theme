<nav class="menu-content">

	<?php 
	$categories = get_categories(); 
	if ($categories) : ?>

		<figure class="menu-column">
    		<figcaption><?php pll_e('categories'); ?></figcaption>
			<ul class="category-list">
		
				<?php foreach($categories as $category) :
				    if ($category->parent == 0  && $category->count > 0 ) { 
				        echo '<li class="cat-item"> 
				        		<a class="bordered" href="' . get_category_link($category->cat_ID) . '" title="' . $category->name . '">' . $category->name . '</a>
				        	</li>';
				    } 
			    endforeach; ?>

		    </ul>
		</figure>

		<figure class="menu-column">
    		<figcaption><?php pll_e('more info'); ?></figcaption>
    		<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container' => false  ) ); ?> 
    		<?php get_template_part("partials/newsletter"); ?>
    		
		</figure>

	<?php endif; ?>

</nav>
