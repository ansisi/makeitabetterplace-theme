<?php

function custom_posts_per_page($query) {
    if ($query->is_category() && $query->is_main_query() ) {
        $query->set( 'nopaging', true );
        $query->set( 'order', 'ASC' );
    }
}

add_action( 'pre_get_posts', 'custom_posts_per_page' );
