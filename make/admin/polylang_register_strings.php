<?php

if (defined('POLYLANG_VERSION')) :

	$context = 'makeitabetterplace-theme'; 
    pll_register_string('categories', 'categories', $context);
    pll_register_string('more info', 'more info', $context);
    pll_register_string('newsletter', 'newsletter', $context);
    pll_register_string('yes!', 'yes!', $context);
    pll_register_string('comments', 'comments', $context);

endif;
