<?php

function makeit_theme_setup()
{
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
    add_theme_support( 'post-thumbnails' );
    $post_type = 'page';
    remove_post_type_support( $post_type, 'post-thumbnails');
    
    add_filter('show_admin_bar', '__return_false');

    update_option('image_default_link_type', 'none');

    add_image_size( 'large', 1920, 1920, false );

    add_image_size( 'medium', 800, 800, false );

    set_post_thumbnail_size( 400, 400, false ); 

}

add_action('init', 'makeit_theme_setup');

add_action( 'init', 'my_add_excerpts_to_pages' );

function my_add_excerpts_to_pages() {

     add_post_type_support( 'page', 'excerpt' );

}

function remove_menus()
{
    // remove_menu_page( 'edit-comments.php' );
}

add_action( 'admin_menu', 'remove_menus' );

function makeit_register_menu()
{
    register_nav_menus(array(
        'main-menu' => __( 'Menu Główne' ),
    ));
}
add_action( 'init', 'makeit_register_menu' );


function insert_menu($theme_location, $menu_depth = 0) {
    wp_nav_menu(array(
        'theme_location' => $theme_location,
        'container' => 'ul',
        'menu_class' => 'menu-list',
        'depth' => $menu_depth,
    ));
}