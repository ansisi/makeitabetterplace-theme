<?php

function kadr_register_menu() {
    register_nav_menus(array(
        'main-menu' => __( 'Menu Główne' ),
        'footer-menu' => __( 'Menu w stopce' ),
    ));
}

add_action( 'init', 'kadr_register_menu' );

function insert_menu($theme_location, $menu_depth = 0) {
    wp_nav_menu(array(
        'theme_location' => $theme_location,
        'container' => '',
        'depth' => $menu_depth,
        'container'     => false,
        'menu_class'    => false,
        'items_wrap'    => '%3$s',
   ));
}
