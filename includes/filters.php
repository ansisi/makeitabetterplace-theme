<?php

function get_category_tree($category_id)
{   
    $category_list = get_categories(array(
        'hide_empty' => false,
        )
    );

    $tree = category_list_to_category_tree($category_list);

    return find_category_in_tree($category_id, $tree);
}

function category_list_to_category_tree($categories)
{

    $tree = array();
    
    foreach ($categories as $category) {
        $tree[$category->parent][] = $category;
    }

    foreach ($categories as $category) if (isset($tree[$category->term_id])) {
        $category->subterms = $tree[$category->term_id];
    }

    return $tree[0];
}

function find_category_in_tree($category_id, $tree)
{   
    foreach ($tree as $term) {
        if ($term->term_id == $category_id)
            return $term;
    }

}

function get_subcategories($category_tree)
{   
    return $category_tree->subterms ? $category_tree->subterms : array();
}
