<?php

function makeitabetterplace_enqueue_styles() {   
    wp_enqueue_style('default', THEME_DIR . '/css/style.css', array());
}

function makeitabetterplace_enqueue_scripts() {
    wp_enqueue_script('main', THEME_DIR . '/js/app.js', array("jquery"), false, true);
    wp_enqueue_script('menu', THEME_DIR . '/js/menu.js', array("jquery"), false, true);
    wp_enqueue_script('svg4everybody', THEME_DIR . '/js/vendors/svg4everybody/svg4everybody.min.js', array("jquery"), false, true);
    wp_enqueue_script('cssvar', THEME_DIR . '/js/vendors/polyfill.cssvar.js', array('jquery'), false, true);
}


add_action('wp_enqueue_scripts', 'makeitabetterplace_enqueue_styles');
add_action('wp_enqueue_scripts', 'makeitabetterplace_enqueue_scripts');
