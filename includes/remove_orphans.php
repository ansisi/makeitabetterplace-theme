<?php

function makeitabetterplace_remove_orphans($text)
{
    global $iworks_orphan;
    return $iworks_orphan->replace($text);
}

if (class_exists('iworks_orphan')) {
    global $iworks_orphan;
    $iworks_orphan = new iworks_orphan();
    
    add_filter('acf/load_value/type=textarea', 'makeitabetterplace_remove_orphans');
}
