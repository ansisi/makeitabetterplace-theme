<?php 
    $post_id = pll_get_post($post->ID, pll_default_language()); 
?>

<article class="post-article <?php makeit_choice_of_background($post_id);?>">

	<div class="article-bg" <?php makeit_post_thumbnail_background(); ?>""></div>

    <a class="link-to-post" href="<?= get_permalink(); ?>" title="<?= the_title(); ?>">
        <h3 class="article-title"><?php the_title(); ?></h3 class="article-title">
    </a>

</article>
