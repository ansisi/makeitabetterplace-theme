<aside class="aside-menu">

    <?php get_template_part("partials/language-switcher"); ?>

    <?php get_template_part("partials/menu-burger"); ?>

    <?php get_template_part("partials/menu-content"); ?>

</aside>