<?php

$newsletter = pll__('newsletter');
$subscribe = pll__('yes!');

if (shortcode_exists('newsletter')) {
    echo do_shortcode("
        [newsletter_form button_label='$subscribe']
        [newsletter_field name='email' placeholder='$newsletter' label='']
        [/newsletter_form]
    ");
}
