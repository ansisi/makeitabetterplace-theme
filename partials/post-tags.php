<h4 class="post-tags">
    <?php

    $post_categories = get_the_category( $post->ID );

    if ( $post_categories ):
        foreach ($post_categories as $category) : 
            echo '<span class="post-category">'.$category->name.'</span>';
        endforeach;
    endif;
    ?>

</h4>
