<?php

if (have_posts()) : ?>

    <section class="posts-section page-content">

    <?php while (have_posts()) : the_post();
        
        get_template_part("partials/post-box");

    endwhile; ?> 

    </section> 

<?php endif;
