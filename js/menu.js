jQuery(function ($) {
   
    $(".menu-burger").on('click', toggleMenu);

    function toggleMenu() {
        $('body').toggleClass('open');
    }

});
